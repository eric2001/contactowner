# ContactOwner
![ContactOwner](./screenshot.jpg) 

## Description
ContactOwner is a module for Gallery 3 which will allow visitors to send email messages to the website owner and/or the person who uploaded the item they are currently viewing.  

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/forum_module_contactowner).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
To install, extract the "contactowner" folder from the zip file into your Gallery 3 modules folder. Afterwards log into your Gallery web site as an administrator and activate the module in the Admin -> Modules menu. Once activated, you will have and Admin -> Settings -> ContactOwner Settings option. Visit this screen to configure the module.  The following options are available.
- **Display Contact Site Owner Link:**  If checked, the ContactOwner sidebar will display a link to send an email to the website's owner (configured below).
- **Display Contact Item Owner Link:**  If checked, the ContactOwner sidebar will display a link to send an email to the Gallery user that created the current album/photo/video.
- **Contact Owner Link Text:**  If "Display Contact Site Owner Link" is checked, this will be the text used for the sidebar link to send messages to the website owner.
- **Owner Email Address:**  If "Display Contact Site Owner Link" is checked, emails sent to the site owner will go to this address.
- **Owner Name:**  If "Display Contact Site Owner Link" is checked, emails sent to the site owner will go to this address.
- **Email Message Header:**  This configurable text will be placed in the top of all messages sent through the ContactOwner module.

This module uses Gallery's built in Sendmail code. To configure this, copy modules/gallery/config/sendmail.php to application/config/sendmail.php and edit the file to set the from and reply_to addresses.  Alternately the PHPMailer module can also be used to allow Gallery to send email messages.

## History
**Version 2.3.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Added logic to display an error if the message fails to send.
> - Released 27 August 2021.
>
> Download: [Version 2.3.0](/uploads/fe6c536c256d200a6e1126014002aebe/contactowner230.zip)

**Version 2.2.0:**
> - Module now includes a link to the album/photo the visitor was viewing before clicking the contact button in the message body.
> - Released 21 March 2011.
>
> Download: [Version 2.2.0](/uploads/364545d4b4576e1ba4a84288a752d9fe/contactowner220.zip)

**Version 2.1.0:**
> - Added support for captchas.
> - Released 07 October 2010.
>
> Download: [Version 2.1.0](/uploads/83baee75d60e8b532aeacde238e86c6d/contactowner210.zip)

**Version 2.0.0:**
> - Updated for recent module API changes in Gallery 3.
> - Users are now required to fill out the entire form before the email will be sent.
> - The module now does some basic checking to make sure the provided email looks valid.
> - Gallery admin's may now specify some header text to be inserted into the top of messages sent using this module.
> - Released 30 December 2009.
>
> Download: [Version 2.0.0](/uploads/332dd5c210e5b48dd4bae6f80efa48db/contactowner200.zip)

**Version 1.3.0:**
> - Changed all references of "user::active()->email" to "identity::active_user()->email" (as per current module API)
> - Tested everything against current git (as of commit e45ea9359d6cb603be0bc28376d92883aa8d7c7e).
> - Released 29 October 2009
>
> Download: [Version 1.3.0](/uploads/df76c7355c5ceb5b3aab12389367289d/contactowner130.zip)

**Version 1.2.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Updated for the new sidebar code in Gallery 3 Git
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 12 October 2009
>
> Download: [Version 1.2.0](/uploads/2f85ce084cac9a52ba2ad8ede1a81de9/contactowner120.zip)

**Version 1.1.0:**
> - Includes bharat's changes for the recent API update.
> - Released 31 July 2009
>
> Download: [Version 1.1.0](/uploads/03da848aa59e852fa9ac2d78abff4638/contactowner110.zip)

**Version 1.0.1:**
> - Fixed issue with line breaks in email message body.
> - Added option to automatically set the "From" line to the logged in user's email.
> - Released on 23 July 2009
>
> Download: [Version 1.0.1](/uploads/6277987623ede273afceb20f3dd204d5/contactowner101.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 22 July 2009
>
> Download: [Version 1.0.0](/uploads/afd2ab84e55b1bdf43212e9161f00d99/contactowner100.zip)
